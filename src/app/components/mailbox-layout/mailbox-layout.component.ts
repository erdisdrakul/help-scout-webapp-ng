import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-mailbox-layout',
    templateUrl: './mailbox-layout.component.html',
    styleUrls: ['./mailbox-layout.component.scss']
})
export class MailboxLayoutComponent implements OnInit {

    constructor() { }

    ngOnInit() {
        $('[data-toggle="tooltip"]').tooltip()
    }

}
