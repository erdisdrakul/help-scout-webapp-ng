import { Component, OnInit } from '@angular/core';
import { ConversationsService } from './../../../services/conversations.service';

@Component({
    selector: 'app-mailbox-list',
    templateUrl: './mailbox-list.component.html',
    styleUrls: ['./mailbox-list.component.scss']
})
export class MailboxListComponent implements OnInit {
    conversations: any[] = [];

    constructor(private conversationsService: ConversationsService) {}

    ngOnInit() {
        this.load();
    }
    
    private async load() {
        this.conversations = await this.conversationsService.find() as any[];
    }

}
