import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxLayoutComponent } from './mailbox-layout.component';

describe('MailboxLayoutComponent', () => {
    let component: MailboxLayoutComponent;
    let fixture: ComponentFixture<MailboxLayoutComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MailboxLayoutComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MailboxLayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
