import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-mail-details',
    templateUrl: './mail-details.component.html',
    styleUrls: ['./mail-details.component.scss']
})
export class MailDetailsComponent implements OnInit {
    htmlMail = `Heya Jarek,<br><br>Oh no! That's no good. The tracking number that was originally sent is working now (36655E64DE), but it doesn't look like it's for your order.<br><br>The correct tracking number is <strong>36655E64DZ</strong>. It looks like there was a hiccup in our system when the tracking number was assigned. Sorry about that!<br><br>It looks like the parcel is on it's way to your specified UK shipping address. If you still haven't received it in 2-3 business days, please let us know.<br><br>Apologies again for the mixup.<br><br>Enjoy your Polka Dot ties! They're one of my favs :)<br><br>Have a great day<br>`;

    @ViewChild('summernoteReply', {static: false}) summernoteReplyRef: ElementRef;
    constructor() { }

    ngOnInit() {
        $('[data-toggle="tooltip"]').tooltip()
    }

    ngAfterViewInit() {
        $(this.summernoteReplyRef.nativeElement).summernote({
            minHeight: 150,
            disableResizeEditor: true
        });
    }

}
