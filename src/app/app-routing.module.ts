import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { MailboxLayoutComponent } from './components/mailbox-layout/mailbox-layout.component';
import { MailboxListComponent } from './components/mailbox-layout/mailbox-list/mailbox-list.component';
import { MailDetailsComponent } from './components/mailbox-layout/mail-details/mail-details.component';


const routes: Routes = [
  {
    path: '', component: MainLayoutComponent,
    children: [
      {
        path: 'mailbox', component: MailboxLayoutComponent,
        children: [
          { path: '', component: MailboxListComponent },
          { path: ':id', component: MailDetailsComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
