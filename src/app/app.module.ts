import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { MailboxLayoutComponent } from './components/mailbox-layout/mailbox-layout.component';
import { MailboxListComponent } from './components/mailbox-layout/mailbox-list/mailbox-list.component';
import { MailDetailsComponent } from './components/mailbox-layout/mail-details/mail-details.component';
import { ConversationsService } from './services/conversations.service';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    MailboxLayoutComponent,
    MailboxListComponent,
    MailDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule

  ],
  providers: [
    ConversationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
