import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ConversationsService {
  constructor(private httpClient: HttpClient) { }

  async find(): Promise<any[]> {
    try {
      const conversations = this.httpClient.get<any[]>(`${environment.apiUrl}/conversations`).toPromise();
      return conversations;
    } catch(e) {
      return [];
    }
  }

}